// Additional typst "syntax" to use in documents
#import "const.typ": *

// Define translations TODO move them into YAML file(s)
#let translations = (
  "en": (
    "truncated": "truncated",
  ),
  "fr": (
    "truncated": "tronqué",
  )
)

#let langs = (
  "py": "Python",
  "sh": "Shell",
)

#let code(
  supplement: [Code],
  code,
  ..sink,
  ) = {
  let caption = none;
  if sink.pos().len() > 0 {
    caption = sink.pos().at(0)
  } else {
    if code.has("children") {
      for child in code.at("children") {
        if child.has("lang") {caption = langs.at(child.at("lang"))}
      }
    }
  }
  figure(
    caption: if caption != none {caption} else {[Code]},
    code,
    supplement: supplement,
  )
}

#let fig(
  supplement: none,
  content,
  ..sink,
  ) = {
  let caption = none;
  let kind = auto;
  if sink.pos().len() > 0 {
    caption = sink.pos().at(0)
  }
  if sink.pos().len() > 1 {
    kind = sink.pos().at(1)
  }
  figure(
    caption: if caption != none {caption},
    content,
    supplement: if supplement != none {supplement} else {auto},
  )
}

#let file(
  text,
  lang: none,
  maxLines: 12,
  maxLength: 84, // Maxi number of chars per line, TODO adapt to font size
  supplement: [Code],
  append: "", // Append to supplement
  start: none,
  start-counter: 0,
  ..sink
  ) = {
  let caption = none
  let lines = text.matches("\n")
  if sink.pos().len() > 0 {
    caption = sink.pos().at(0)
  } else if lang != none and lang.has(lang) {
    caption = langs.at(lang)
  }
  // Crop content at a certain starting point & define lines
  if type(start) == "string" {
    let start-int = text.matches(start).at(start-counter).at("start")
    if start-int != none {start = start-int}
  }
  text = text.slice(start)
  lines = text.matches("\n")
  // Get string corresponding to indentation of first line
  let indent = text.match(regex("^ *")).at("text")
  if lines.len() > maxLines {
    // Crop text if too long
    text = text.slice(0, lines.at(maxLines - 1).at("start"))
    // Remove indentation at beginning of lines
    text = text.replace(regex("(?m)^"+indent), "")
  }
  // Crop too long lines
  for l in text.matches(regex("(?m)^(.{1," + str(maxLength) + "}).*$")) {
    if l.at("text").len() - indent.len() > maxLength {
      text = text.replace(l.at("text"), l.at("captures").at(0))
    }
  }
  figure(
    caption: caption,
    raw(text, block: true, lang: lang),
    supplement: supplement + append,
  )
}
