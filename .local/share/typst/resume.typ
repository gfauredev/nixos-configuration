// General resume Typst template, originally created for my resume
#import "lib.typ": *

#let template(
  author,
  title,
  subtitle: none,
  lang: "en",
  title-color: rgb(26%, 26%, 34%),
  left-bar: none,
  left-color: luma(230),
  left-bar-color: rgb(18%, 20%, 22%),
  center-color: luma(230),
  right-bar: none,
  right-color: luma(230),
  right-bar-color: rgb(10%, 30%, 80%),
  margins: .5cm,
  size: 12pt,
  bar-size: 12pt,
  body-width: 2fr,
  paper: "a4",
  space: .5em,
  one-page: true,
  body,
) = {
  // SETTINGS
  set document(author: author, title: "CV - " + author)
  set text(
    lang: lang,
    size
  )
  set block(
    spacing: space,
    //above: space,
    //below: space * 1.2
  )
  set par(
    leading: space,
    justify: true
  )
  set page(
    paper: paper,
    margin: (
      top: margins * 1.5,
      rest: margins
    ),
    // header-ascent: 0%
  )
  set list(
    marker: none
  )
  show link: underline
  // h1 with underline
  show heading.where(level: 1): h1 => {
    set text(font: serif-font)
    set block(above: space * 1.75)
    h1
    set block(above: space, below: space * 1.25)
    line()
  }
  // h3 a bit smaller, italic & indented
  show heading.where(level: 3): h3 => {
    set text(
      font: sans-font,
      weight: 550,
      size: .9em,
      style: "italic",
    )
    set block(above: space)
    move(dx: size, h3)
  }
  // Lower space above headings
  //show heading: h => {
    //set block(above: space)
    //h
  //}

  // Author + Title header
  let header = align(center, {
    set text(fill: title-color)
    set block(spacing: size)
    text(2.8em, weight: 600, author)
    line(length: 60%)
    text(font: sans-font, 1.6em, title)
    parbreak()
    text(font: sans-font, 1.2em, subtitle)
    parbreak()
  })

  // Define document columns according to input data
  let doc-columns = ()
  if left-bar != none {
    if right-bar != none {
      doc-columns = (1fr, body-width, 1fr)
    } else {
      doc-columns = (1fr, body-width)
    }
  } else {
    if right-bar != none {
      doc-columns = (body-width, 1fr)
    } else {
      doc-columns = (body-width)
    }
  }

  // Define main body content
let body = {
  set text(font: serif-font)
  // h2 with beginning circle in body
  show heading.where(level: 2): h2 => block(
    above: space * 1.5,
    {
      set text(font: sans-font)
      place(start, sym.circle)
      move(dx: size * 1.25, h2)
    }
  )
  set line(start: (space, 0pt), length: 100% - space)
  v(1fr)
  body
  v(1fr, weak: true)
}

  // Define left bar content with settings
  left-bar = {
    // Reduce space around headings
    show heading.where(level: 2): h => {
      set block(above: space * 1.5, below: space)
      h
    }
    set text(
      fill: left-color,
      font: sans-font,
      size: bar-size,
    )
    set line(
      start: (space, 0pt),
      length: 100% - space / 3.6,
      stroke: left-color
    )
    // Display lists in 2 columns
    show list: columns
    v(1fr)
    left-bar
    v(2fr, weak: true)
  }

  // DEBUG
  //repr(body)

  // Side bar + Main body
  block(
    breakable: not one-page,
    {
      header
      grid(
        columns: doc-columns,
        gutter: space,
        block(
          fill: left-bar-color,
          inset: space,
          radius: space * 2,
          width: 100%,
          left-bar
        ),
        block(
          width: 100%,
          body
        ),
        right-bar
      )
    }
  )
}
