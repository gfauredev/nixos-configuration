// Fonts
#let sans-font = ("Inter", "FiraGO", "sans-serif")
#let serif-font = ("Libre Baskerville", "Merriweather", "Vollkorn", "serif")
#let mono-font = ("FiraCode Nerd Font", "mono")

// Components
#let signField = block(
  width: 160pt,
  height: 40pt,
  above: 0pt,
  fill: luma(230),
  radius: 4pt,
)

