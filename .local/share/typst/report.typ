// General report Typst template, originally created for an internship report
#import "lib.typ": *
#import "shortcuts.typ": *

#let template(
  lang: "en",
  title: none,
  subtitle: none,
  dates: none,
  authors: (:),
  people: (:),
  titlepages: (),
  special-pages: (:),
  toc: true,
  tof: none,
  glossaryTitle: none,
  glo: none,
  bib: none,
  header: true,
  footer: true,
  ascent: .25cm,
  descent: .25cm,
  margin-top: 1.5cm,
  margin-right: 2cm,
  margin-bottom: 2cm,
  margin-left: 2cm,
  paper: "a4",
  flipped: false,
  body,
) = {
  // SETTINGS
  set document(author: authors.values(), title: title)
  set page(
    paper: paper,
    flipped: flipped,
    margin: (
      top: margin-top + ascent,
      right: margin-right,
      bottom: margin-bottom + descent,
      left: margin-left,
    ),
  )
  set text(font: serif-font, lang: lang)
  set par(
    leading: .65em
  )
  set block(
    above: 1em,
    below: 1.2em,
  )
  show link: underline
  // show list: set block(breakable: false)
  // set pagebreak(weak: true)

  // Headings
  set heading(numbering: "1.A.a 1.A.a") if toc
  show heading.where(level: 1): h => align(center, h)
  show heading: set text(font: sans-font)

  // Code
  show raw: set text(font: mono-font, overhang: false)
  show raw.where(block: false): box.with(
    fill: luma(220),
    inset: (x: 3pt, y: 0pt),
    outset: (y: 3pt),
    radius: 2pt,
  )
  show raw.where(block: true): r => {
    set align(left)
    set par(justify: false)
    let lines = r.at("text").matches("\n").len()
    let l = 0
    let nb = ""
    while l <= lines {
      l += 1
      nb += str(l) + "\n"
    }
    block(
      width: 100%,
      fill: luma(220),
      inset: 1em,
      radius: .5em,
      clip: true,
      breakable: false,
      grid(
        columns: (auto, 1fr),
        gutter: .25em,
        text(font: mono-font, fill: luma(60), nb),
        r
      )
    )
  }

  // Images
  // show image: block.with(clip: true, radius: .5em) // Round images corners

  // CONTENT
  // Title pages
  for titlepage in titlepages {
    if titlepage not in (none, false) {
      titlePage(
        logo: if type(titlepage) == "dictionary"
          and "logo" in titlepage {
            titlepage.logo
          },
        people: authors + people,
        dates: dates,
        title: title,
        subtitle: subtitle,
        footer: if type(titlepage) == "dictionary"
          and "footer" in titlepage {
            titlepage.footer
          },
      )
    }
    pagebreak()
  }

  // Special pages (like thanks)
  for (title, content) in special-pages {
    heading(title, numbering: none)
    content
    pagebreak()
  }

  // Table of contents
  if toc {
    locate(loc => {
      if counter(heading).final(loc).at(0) > 0 {
        outline(depth: 3, indent: true)
      }
      pagebreak()
    })
  }

  // Table of figures
  if type(tof) == "string" {
    locate(loc => {
      if counter(figure).final(loc).at(0) > 0 {
        outline(
          title: tof,
          target: figure,
        )
        // pagebreak() // Disabled as the set below creates a page
      }
    })
  }

  // Remove empty characters used by glossary
  // WARNING may cause memory overflow if glossaryWords is executed after
  show sym.zwj: ""

  // Main body, with page headers, footers & numbering
  {
    // HACKY but automatic glossary linking
    show glossaryWords(glo): word => glossaryShow(glo, word)

    set page(
      header: if header {
        set text(size: 10pt)
        set block(above: 0pt, below: .75em)
        grid(columns: 2, gutter: 1fr,
          title,
          subtitle
        )
        line(length: 100%)
      } else {none},
      header-ascent: .25cm,
      footer: if footer {
        set text(size: 10pt)
        set block(above: .75em, below: 0pt)
        line(length: 100%)
        grid(columns: 2, gutter: 1fr,
          authors.values().at(0),
          counter(page).display(
            "1 / 1",
            both: true,
          )
        )
      } else {none},
      footer-descent: .25cm,
    )
    set par(justify: true)
    show heading.where(level: 1): h => {pagebreak(weak: true); h}
    body
  }

  // Glossary
  if type(glo) == "dictionary" {
    pagebreak()
    glossary(glo, title: glossaryTitle)
  }

  // Bibliography
  if bib != none {
    pagebreak()
    bib
  }
}
