let
  rustOverlay = import (
    builtins.fetchTarball
      "https://github.com/oxalica/rust-overlay/archive/master.tar.gz"
  );
  # pinnedPkgs = nixpkgs.fetchFromGitHub {
  #   owner = "NixOS";
  #   repo = "nixpkgs";
  #   rev = "xxx";
  #   sha256 = "xxx";
  # };
  # pkgs = import pinnedPkgs { overlays = [ (import rustOverlay) ]; };
  pkgs = import <nixpkgs> { overlays = [ rustOverlay ]; };
  rustVersion = "latest";
  rust = pkgs.rust-bin.stable.${rustVersion}.default.override
    # rust = pkgs.rust-bin.nightly.latest.default.override
    {
      extensions = [
        "rust-src" # for rust-analyzer
      ];
    };
  packages = with pkgs; [
    cargo-watch
    gcc
    glibc
    glib
    gnumake
    cmake
    rust-analyzer
  ];
in
pkgs.mkShell
{
  name = "rust";
  buildInputs = [ rust ] ++ (packages);
  LD_LIBRARY_PATH = pkgs.lib.makeLibraryPath packages;
  RUST_BACKTRACE = 1;
  shellHook = "exec zsh";
}
